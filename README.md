# Python/SageMath implementation of pairings on BN, BLS12, BLS24 and SNARK-friendly 2-chains of curves


This repository contains the following files. All files are written in
Python/SageMath and found under `sage/` except a Magma script in `magma/`.

For generating Brezing--Weng curves:

  - `find_all_2chain_BLSn_BWk.sage`

It generates outer Brezing-Weng curves of embedding degree 6, with inner curves
BLS12 (Barreto-Lynn-Scott), BLS24, or BN (Barreto-Naehrig).

Test vectors of curves:

 -  With BLS-BW6:
    - `testvector_bls12_377_bw6_768.py`
    - `testvector_bls12_379_bw6_768.py`
	- `testvector_bls12_381_bw6_768.py`
    - `testvector_bls24_315_bw6_640.py`
    - `testvector_bls24_315_bw6_672.py`
 -  With BLS12-CP8:
    - `testvector_bls12_377_cp8d1_768.py`
    - `testvector_bls12_379_cp8d1_768.py`
 -  With BLS12-CP12:
    - `testvector_bls12_377_cp12d3_768.py`
    - `testvector_bls12_379_cp12d3_768.py`
 -  With BLS24-CP:
    - `testvector_bls24_315_cp12d3_640.py`
    - `testvector_bls24_315_cp8d1_640.py`
 -  With BN-BW6:
    - `testvector_bn_253_bw6_512.py`
    - `testvector_bn_254_bw6_512.py` BW6 curves obtained from the inner curve
      BN-254 with seed _u_ = `-(2^62+2^55+1)`, suggested in
      [ePrint 2010/429](https://eprint.iacr.org/2010/429)
      and used in Geppetto,
      [ePrint 2014/976](https://eprint.iacr.org/2014/976).
    - `testvector_bn_254e_bw6_516.py` BW6 curves obtained from the inner curve
      BN-254 of Ethereum with seed `0x44e992b44a6909f1` so that _r_-1 has
      2-valuation 28.
    - `testvector_bn_382_bw6_767.py`
    - `testvector_bn_446_bw6_896.py` BW6 curves obtained from the inner curve
      BN-446 [Pluto](https://github.com/daira/pluto-eris/) with seed
      _u_=`-0x4000000000001000008780000000`.

For the formulas of optimal ate pairing, optimal twisted ate pairing, optimized
hard part of the final exponentiation like in _Faster hashing to_ **G**2 at
[SAC'2011](https://doi.org/10.1007/978-3-642-28496-0_25) and
_On the Final Exponentiation in Tate Pairing Computations_ at
[IEEE-TIT 59 (6) 2013](https://doi.org/10.1109/TIT.2013.2240763),
fast co-factor multiplication on **G**1, on **G**2, multiplication by a multiple
of _r_ on **G**1, on **G**2:

 -  `magma/script_formulas_bw6_bls12_bls24_bn.mag`

Pairing implementation:

  - `pairing.py` mostly from
    [Gitlab SageMath BW6-761](https://gitlab.inria.fr/zk-curves/bw6-761).
    There is no specific file `pairing_bls.py` because on BLS curves the optimal
    ate pairing has the same Miller loop equation as the ate pairing:
    _f_{_x_,_Q_}(_P_) = _f_{_t_-1,_Q_}(_P_) since the trace has form
    _t_(_x_) = _x_+1.
    The hard part of the final exponentiation is specific for each embedding
    degree and is given at the end of the file for BLS-9, BLS-12, BLS-15,
    BLS-24, BLS-27, BLS-48. The fastest formulas so far are in
    [ePrint 2020/875](https://eprint.iacr.org/2020/875).
  - `pairing_bn.py` Miller loop and final exponentiation on BN curves.
  - `pairing_bw6_bn.py` for BW6-BN curves.
  - `pairing_bw6_bls12.py` for BW6-BLS12 curves.
  - `pairing_bw6_bls24.py` for BW6-BLS12 curves.
  - `pairing_cocks_pinch.py` for Cocks-Pinch curves.

Tests, main files, run `sage $FILE`:

  - `test_pairing_bls12.py` (BLS12-377, BLS12-379, BLS12-381, BLS12-383)
  - `test_pairing_bls24_318.py`
  - `test_pairing_bn.py`
  - `test_pairing_bw6_761.py`
  - `test_pairing_bw6_bls12.py`
  - `test_pairing_bw6_bls24.py`
  - `test_pairing_cp12_bls12.py`
  - `test_pairing_cp12_bls24.py`
  - `test_pairing_cp8_bls12.py`
  - `test_pairing_cp8_bls24.py`
  - `test_pairing_bw6_bn.py`

Auxiliary test files:

  - `test_pairing.py`
  - `test_scalar_mult.py`
  - `test_pairing_bw6.py`

Test of the distortion map on BLS curves on the subgroup of the co-factor
(_x_-1)^2/3:

  - `test_bls_distortion_map.py`

Estimates of costs of pairings:

  - `cost_pairing.py`
